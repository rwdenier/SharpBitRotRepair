﻿/*  SharpBitRotRepair computes parity information, and if the original file is corrupted can try to repair it.
    Copyright (C) 2022 Robert Denier

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

using RWD_Common.Files;
using RWD_Common.Misc.Constants;

using SharpBitRotChecker;
using SharpBitRotChecker.Enumerations;
using SharpBitRotChecker.Miscellaneous;
using SharpBitRotChecker.Reader;
using SharpBitRotRepair.Algorithm;

namespace SharpBitRotRepair
{
    public partial class MainWindow : Window
    {
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();
        private List<ErrorInfo> errors = new List<ErrorInfo>();

        public MainWindow()
        {
            InitializeComponent();
            this.grid.Margin = CommonConstants.DefaultMargin;
            log.Info("SharpBitRotRepair version 0.1, Copyright(C) 2022 Robert Denier.  " +
            "SharpBitRotRepair comes with ABSOLUTELY NO WARRANTY.  " +
            "This is free software, and you are welcome to redistribute it under certain conditions.  " +
            "For details refer to LICENSE.txt.\r\n\r\n" +
            "  /*Users are cautioned to backup files before use, particularly if attempting a repair.*/");
            log.Info("Source code is available at https://gitlab.com/rwdenier/SharpBitRotRepair.");
            log.Info("To use first select the folder which contains files that you want to generate " +
                "parity files for.  Then select where to put the parity files.  This should be a different " +
                "folder.  Then click the appropriate button.");
#if DEBUG
            this.scanFolder.File = @"C:\Temp\Data";
            this.parityFolder.File = @"C:\Temp\Parity";
#endif
        }

        public void ScanDelegate(object options, object options2, string source_file, string target_file)
        {
            //Update this to a more appropriate name.
            target_file = target_file.ToParityFileName();
            ParseMode parseMode = ParseMode.CheckParityFiles;
            bool overwriteOrRepair = false;
            if (options is ParseMode)
            {
                parseMode = (ParseMode)options;
            }
            if (options2 is bool)
            {
                overwriteOrRepair = (bool)options2;
            }
            List<ParseFileError> list = null;
            if (parseMode == ParseMode.CheckParityFiles)
            {
                list = CheckParity.ProcessFile(source_file, target_file);
            }
            else
            {
                list = CreateParity.ProcessFile(source_file, target_file, overwriteOrRepair);
            }
            if (list != null && list.Count > 0)
            {
                ErrorInfo errorInfo = new ErrorInfo(source_file, target_file, list);
                lock (errors)
                {
                    errors.Add(errorInfo);
                }
                ////This could possibly be the starting point of a repair.
                //foreach(ParseFileError item in list)
                //{
                //    log.Error(item.ToString());
                //}

            }
        }



        private void Start(ParseMode parseMode, bool overwriteOrRepair, int min_file_size, string start_path, string parity_path)
        {
            var t = Task.Run(
                () => FileScanner.Scan(parseMode, overwriteOrRepair, min_file_size, start_path, parity_path, ScanDelegate)
            );
            t.Wait();

            //origin of errors?
            if (parseMode == ParseMode.CheckParityFiles && overwriteOrRepair && errors.Count > 0)
            {
                foreach (ErrorInfo v in errors)
                {
                    log.Info(v.ToString());
                    RepairFile.ProcessFile(v);
                }
            }
            log.Info("Finished.");
            SetButtonsEnabledStateThreadSafe(true, "");
        }

        private void CreateParity_Click(object sender, RoutedEventArgs e)
        {
            bool overwrite = (bool)this.overwriteBox.IsChecked;
            string start_path = this.scanFolder.File;
            string parity_path = this.parityFolder.File;
            int min_file_size = 1024;
            if (overwrite)
            {
                SetButtonsEnabledState(false, "Creating Parity Files - Existing files will be replaced.");
            }
            else
            {
                SetButtonsEnabledState(false, "Creating Parity Files");
            }
            Task.Run(
                () => Start(ParseMode.CreateParityFiles, overwrite, min_file_size, start_path, parity_path)
            );
        }

        private void CheckParity_Click(object sender, RoutedEventArgs e)
        {
            bool repair = (bool)this.repairBox.IsChecked;
            string start_path = this.scanFolder.File;
            string parity_path = this.parityFolder.File;
            int min_file_size = 1024;
            if (repair)
            {
                SetButtonsEnabledState(false, "Checking and Attempting Repairs as Needed");
            }
            else
            {
                SetButtonsEnabledState(false, "Checking Files");
            }
            Task.Run(
                () => Start(ParseMode.CheckParityFiles, repair, min_file_size, start_path, parity_path)
            );
        }

        private void SetButtonsEnabledStateThreadSafe(bool enabled, string message)
        {
            Application.Current.Dispatcher.BeginInvoke(
              () => SetButtonsEnabledState(enabled, message));
        }

        private void SetButtonsEnabledState(bool enabled, string message)
        {
            if (enabled)
            {
                this.buttonsGrid.Visibility = Visibility.Visible;
                this.Title = "Sharp Bit Rot Repair";
            }
            else
            {
                this.buttonsGrid.Visibility = Visibility.Collapsed;
                this.Title = "Sharp Bit Rot Repair : " + message;
            }
        }

        /// <summary>
        /// This is being deliberately hardcoded to only corrupt c:/Temp/Data/sbrr.avi to make it very difficult to corrupt anyones files accidentally.
        /// This is only for testing.  A video file is a good choice since it may still play, but you can see the corruption, then verify it is not there after the repair.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CorruptFile_Click(object sender, RoutedEventArgs e)
        {
            string path = @"C:/Temp/Data/sbrr.avi";
            CorruptFile.ProcessFile(path, 0 /*chunk*/, 3 /* corruptionsPerShard*/, 650 /*numShards*/);
        }
    }
}
