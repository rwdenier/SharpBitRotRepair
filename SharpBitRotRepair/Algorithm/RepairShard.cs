﻿/*  SharpBitRotRepair computes parity information, and if the original file is corrupted can try to repair it.
    Copyright (C) 2022 Robert Denier

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
using RWD_Common.Math;
using SharpBitRotChecker.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SharpBitRotRepair.Algorithm
{
    public class RepairShard
    {
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        //Repair Algorithm Cautions 
        //1. One can't just search sets of indexes, solving for all 4 values till something works out.
        //2. This will fail every time, unless you just happen to contain the errors in that set.
        //3. The reason for this is the algorithm would assume the values you happened to replace were unknowns
        //   and the values that are actually erroneous are more unknowns, therefore tehre is not enough information to solve.


        /// <summary>
        /// Shard Repair is not guaranteed.  This code is AS IS with no warranty of any kind.
        /// This is not proved mathmatically.
        /// </summary>
        /// <param name="shardData"></param>
        /// <param name="parity"></param>
        /// <returns></returns>
        public static bool Repair(Int64[] shardData, Column4x1 parity)
        {
            Int64[] testShard = new Int64[shardData.Length];
            int maxCount = ShardRepairPaths.Sets.Count;
            int currentMismatchCount = int.MaxValue;
            List<RepairInformation> bestMatches = new List<RepairInformation>();

            //Fixing the Repair Algorithm.
            //1. A correct answer must match one possible solution value.
            //2. A correct answer must have the four values that are returned in the range of an unsigned 32.
            //3. This means that negative values or values above 2^32-1 are rejected as a possible solution.
            //4. Any solution that meets these criteria will be considered.

            //Refining stage 1.
            //1. If there is only one solution, then no refinement is necessary.
            //2. First the set calculated in the previous should be reduced to the set of changes.
            //3. If two or more possible solutions contain the same change, the solutions are equivalent and the extras can be discarded.
            //4. If at this point there is only one solution, then that should repair the shard.

            //TODO: Refining stage 2.
            //1. In the event there is still more than one solution, take the set of changes and calculate which one results in fewer bits changed.
            //2. Then take that solution.

            //Note: I'm not explicitly proving that this is providing correct solutions.  Use at your own risk.
            foreach (ShardRepairSet set in ShardRepairPaths.Sets)
            {
                //This is basically subtracting from the parity data the contribution from the rest.
                //The idea is to reduce the problem to something that can be solved.
                Column4x1 rightHandSide = GetUpdatedRHS(set.NotSelectedIndexes, shardData, parity);
                Column4x1 possibleNewValues = set.InvertedMatrix.Multiply(rightHandSide);
                if (possibleNewValues.CheckIfPossiblyValidData())
                {
                    //Take the new values and put then into a test shard.
                    GetTestShard(testShard, set.SelectedIndexes, shardData, possibleNewValues);
                    //Now recalculate the parity and see if it is now correct.
                    Column4x1 new_parity = RecalcParity(testShard);
                    if (new_parity.Equals(parity))
                    {
                        Column4x1 oldData = GetOldData(set.SelectedIndexes, shardData);
                        string mismatchLocations = possibleNewValues.GetMismatchString(oldData, set.SelectedIndexes, out int mismatchCount);
                        if (mismatchCount < currentMismatchCount)
                        {
                            bestMatches.Clear();
                            currentMismatchCount = mismatchCount;
                        }
                        if (mismatchCount == currentMismatchCount)
                        {
                            if (!(bestMatches.Where(x=>x.mismatchLocations == mismatchLocations).Any()))
                            {
                                bestMatches.Add(new RepairInformation(set, oldData, possibleNewValues, mismatchCount, mismatchLocations));
                            }
                        }
                    }
                }
            }
            if (bestMatches.Count == 1)
            {
                var b = bestMatches[0];
                //Regenerating this here is probably quicker than allocating the memory to potentially store multiple solutions.
                GetTestShard(testShard, b.set.SelectedIndexes, shardData, b.possibleValues);
                Array.Copy(testShard, shardData, shardData.Length);
                return true; //done
            } else
            {
                //During one failure when a match should have existed there were 0 matches.
                /* failure */
                log.Error("Failed to repair shard.  " + bestMatches.Count + " matches remain.");
                return false;
            }
        }

        internal class RepairInformation
        {
            public string mismatchLocations;
            public ShardRepairSet set;
            public Column4x1 oldValues;
            public Column4x1 possibleValues;
            public int mismatchCount;

            public RepairInformation(ShardRepairSet set, Column4x1 oldValues, Column4x1 possibleValues, int mismatchCount, string mismatchLocations)
            {
                this.set = set;
                this.oldValues = oldValues;
                this.possibleValues = possibleValues;
                this.mismatchCount = mismatchCount;
                this.mismatchLocations = mismatchLocations;
            }
        }

        /// <summary>
        /// This simplifies the A*B=C calculation such that the values we are not trying to solve for are simplified.
        /// This creates a new right hand side to replace parity.
        /// </summary>
        /// <param name="notSelectedIndexes"></param>
        /// <param name="shardData"></param>
        /// <param name="parity"></param>
        /// <returns></returns>
        private static Column4x1 GetUpdatedRHS(Int32[] notSelectedIndexes, Int64[] shardData, Column4x1 parity)
        {
            Column4x1 tmpSet = new Column4x1();
            tmpSet.valid = true;
            foreach (var index in notSelectedIndexes)
            {
                tmpSet.p += Constants.p_constants[index] * shardData[index];
                tmpSet.q += Constants.q_constants[index] * shardData[index];
                tmpSet.r += Constants.r_constants[index] * shardData[index];
                tmpSet.s += Constants.s_constants[index] * shardData[index];
            }
            parity.p -= tmpSet.p;
            parity.q -= tmpSet.q;
            parity.r -= tmpSet.r;
            parity.s -= tmpSet.s;
            return parity;
        }

        private static Column4x1 RecalcParity(Int64[] testShard)
        {
            Column4x1 column4X1 = new Column4x1();
            for (Int64 word = 0; word < Constants.WordsPerShard; word++)
            {
                column4X1.p += Constants.p_constants[word] * testShard[word];
                column4X1.q += Constants.q_constants[word] * testShard[word];
                column4X1.r += Constants.r_constants[word] * testShard[word];
                column4X1.s += Constants.s_constants[word] * testShard[word];
            }
            return column4X1;
        }

        private static void GetTestShard(Int64[] testShard, Int32[] indexes, Int64[] shardData, Column4x1 newValues)
        {
            //source 
            Array.Copy(shardData, testShard, shardData.Length);
            testShard[indexes[0]] = newValues.p;
            testShard[indexes[1]] = newValues.q;
            testShard[indexes[2]] = newValues.r;
            testShard[indexes[3]] = newValues.s;
        }

        private static Column4x1 GetOldData(Int32[] indexes, Int64[] testShard)
        {
            Column4x1 column4X1 = new Column4x1();
            column4X1.p = testShard[indexes[0]];
            column4X1.q = testShard[indexes[1]];
            column4X1.r = testShard[indexes[2]];
            column4X1.s = testShard[indexes[3]];
            return column4X1;
        }

    }
}
