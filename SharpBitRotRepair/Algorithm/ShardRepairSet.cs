﻿/*  SharpBitRotRepair computes parity information, and if the original file is corrupted can try to repair it.
    Copyright (C) 2022 Robert Denier

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
using RWD_Common.Math;
using System;

namespace SharpBitRotRepair.Algorithm
{
    public class ShardRepairSet
    {
        /// <summary>
        /// This is the set of indexes to be set as unknowns to try to find and repair the error.
        /// </summary>
        public Int32[] SelectedIndexes;
        /// <summary>
        /// The is the set of indexes that are left as is.
        /// </summary>
        public Int32[] NotSelectedIndexes;

        public Matrix4x4 InvertedMatrix;

        public ShardRepairSet(string csv)
        {
            this.SelectedIndexes = new Int32[4];
            this.NotSelectedIndexes = new Int32[28];

            string[] strings = csv.Split(',');

            this.SelectedIndexes[0] = int.Parse(strings[0]);
            this.SelectedIndexes[1] = int.Parse(strings[1]);
            this.SelectedIndexes[2] = int.Parse(strings[2]);
            this.SelectedIndexes[3] = int.Parse(strings[3]);

            int idx = 0;
            for(int i = 0; i < 32; i++)
            {
                if (Array.IndexOf(this.SelectedIndexes,i) < 0)
                {
                    this.NotSelectedIndexes[idx] = i;
                    idx++;
                }
            }
        }

    }
}
