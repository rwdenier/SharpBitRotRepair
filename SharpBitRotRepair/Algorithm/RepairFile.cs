﻿/*  SharpBitRotRepair computes parity information, and if the original file is corrupted can try to repair it.
    Copyright (C) 2022 Robert Denier

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
using RWD_Common.Math;
using SharpBitRotChecker.Algorithm;
using SharpBitRotChecker.Configuration;
using SharpBitRotChecker.Reader;
using System;
using System.Collections.Generic;
using System.IO;

namespace SharpBitRotRepair.Algorithm
{
    /// <summary>
    /// This is the class that is used to attempt to repair a file.
    /// </summary>
    public class RepairFile : ParseFile
    {
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();
        private BinaryReader parityReader;

        private readonly ErrorInfo errorInfo;


        /// <summary>
        /// NOTE:  Make sure only the actual number are recorded/used.
        /// </summary>
        private Int64[] Shard = new Int64[Constants.WordsPerShard];


        private RepairFile(ErrorInfo errorInfo) : base (errorInfo.SourceFile, true /* allow write */)
        {
            this.errorInfo = errorInfo;
            log.Warn("Attempting repair on <" + errorInfo.SourceFile + ">...");
        }

        private bool OpenParityFile(string parityFile)
        {
            if (File.Exists(parityFile))
            {
                this.parityReader = new BinaryReader(File.OpenRead(parityFile));
                return true;
            }
            else
            {
                log.Error(parityFile + " does not exist.  Nothing to do...");
                return false;
            }
        }



        /// <summary>
        /// The goal of this is simply to check if things match.  
        /// If they don't they can be stored into a file which will allow
        /// the end user to determine how to proceed.  Repairing the file
        /// is expected to be slower, and while only work within the limit
        /// of the design.
        /// 
        /// This should abort scanning of the file once a failure is detected.
        /// It is worth noting that the first action an end user may take
        /// is to backup everything else, though the default action will
        /// be to continue to scan the other files.
        /// </summary>
        private Column4x1 GetParityChunk(Int32 chunk, Int32 required_shard)
        {
            int numShards = this.settings.NumShards;
            int bytesPerShard = 3 * 8;
            Int64 bytes_per_chunk = bytesPerShard * numShards;
            Int64 offset = bytes_per_chunk * chunk;
            Int64 extra_shard_offset = bytesPerShard * required_shard;
            offset += extra_shard_offset;
            /*** Reposition to start of next read ***/
            this.parityReader.BaseStream.Position = offset;
            var w1 = this.parityReader.ReadUInt64();
            var w2 = this.parityReader.ReadUInt64();
            var w3 = this.parityReader.ReadUInt64();
            WorkingSetCompressed workingSetCompressed= new WorkingSetCompressed(w1,w2,w3);
            return workingSetCompressed.GetWorkingSet();
        }


        private bool RepairTheFile()
        {
            if (!OpenParityFile(this.errorInfo.ParityFile))
            {
                return false;
            }
            base.ResetTimer();
            int failures = 0;
            foreach (ParseFileError x in errorInfo.errors)
            {
                base.SetRegion(x.chunk);
                //Need:
                //1. The data shard, so 32 - 32 bit numbers.
                //2. The 4 values for the parity.
                
                //Get the shard
                base.GetShard(x.shard, this.Shard);
                int shard_old = x.shard;
                //We do not know the exact bits that are wrong.  
                //It could even be that the parity bytes are wrong.
                //In order to repair this the next step is to get the parity chunk.
                var parityChunk = this.GetParityChunk(x.chunk, x.shard);

                //This takes the shard and fixes it, or at least tries to.  It does not yet write back to the disk.
                bool rVal = RepairShard.Repair(this.Shard, parityChunk);
                if (rVal)
                {
                    //Commit the repair to disk.
             //       log.Warn("Repaired shard " + x.shard + " in chunk " + x.chunk + ".");
                    base.SetShard(x.shard, this.Shard);
                } else
                {
                    failures++;
                }
                base.UnsetRegion();
                base.LogNowAndThen(this.errorInfo.SourceFile, x.chunk, x.shard);
                if (tmpErrors.Count > 0)
                {
                    log.Error("<" + errorInfo.SourceFile + "> has at least one error.");
                    base.ParseFileErrors.AddRange(tmpErrors);
                }
            }
            this.Dispose();
            if (failures > 0)
            {
                log.Error("Failed to repair " + failures + " out of " + errorInfo.errors + " errors.");
                return false;
            }
            return true;
        }

        public static List<ParseFileError> ProcessFile(ErrorInfo errorInfo)
        {
            RepairFile create = new RepairFile(errorInfo);
            create.RepairTheFile();
            return create.ParseFileErrors;
        }


        new private void Dispose()
        {
            base.Dispose();
            if (this.parityReader != null)
            {
                this.parityReader.Close();
                this.parityReader.Dispose();
                this.parityReader = null;
            }
        }

    }
}
