﻿/*  SharpBitRotRepair computes parity information, and if the original file is corrupted can try to repair it.
    Copyright (C) 2022 Robert Denier

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
using RWD_Common.Math;
using SharpBitRotChecker.Configuration;
using System;
using System.Collections.Generic;

namespace SharpBitRotRepair.Algorithm
{
    public static class ShardRepairPaths
    {
        /// <summary>
        /// This should be the minimal to reduce the search space.
        /// It is computed once so efficiency is of minimal importance.
        /// What can be precomputed is precompuuted.
        /// </summary>
        static public List<ShardRepairSet> Sets = new List<ShardRepairSet>();

        static ShardRepairPaths()
        {
            CalcRepairPaths();
            foreach(ShardRepairSet set in Sets)
            {
                Matrix4x4 matrix4X4 = GetMatrix(set.SelectedIndexes);
                //The determinate contribution is kept separate to keep this lossless. 
                Matrix4x4 inverted = matrix4X4.PartialInverse();
                set.InvertedMatrix = inverted;
            }
        }

        private static void CalcRepairPaths()
        {
            HashSet<string> tmp = new HashSet<string>();
            int[] arr = new int[4];
            for (int i = 0; i < 32; i++)
            {
                for (int j = 0; j < 32; j++)
                {
                    for (int k = 0; k < 32; k++)
                    {
                        for (int l = 0; l < 32; l++)
                        {
                            if (i != j && i != k && i != l &&
                                        j != k && j != l &&
                                        k != l)
                            {
                                arr[0] = i;
                                arr[1] = j;
                                arr[2] = k;
                                arr[3] = l;
                                Array.Sort(arr);
                                tmp.Add(string.Join(',', arr));
                            }
                        }
                    }
                }
            }
            foreach (var csv in tmp)
            {
                ShardRepairSet shardRepairSet = new ShardRepairSet(csv);
                Sets.Add(shardRepairSet);
            }
        }


        /// <summary>
        /// Gets the coefficients that are linked to the remaining unknowns.
        /// Used in ShardRepairPaths.cs
        /// </summary>
        /// <param name="selected_indexes">This usese Column4x1 for efficiency, but is just 4 selected indexes.</param>
        /// <returns></returns>
        private static Matrix4x4 GetMatrix(Int32 [] selected_indexes)
        {
            Matrix4x4 m = new Matrix4x4();

            /* The value in selected indexes allows the constants correlating to the linear system to be selected.
             * selected_index[0] - selected_index[3] select 4 columns, thus four pieces of data from the system.
             * 
             * Normally to fill out the complete left hand side of the set of linear equations you would have
             * 4 rows by 32 columns.
             * 
             * In order to solve the system it is reduced to 4 rows and 4 columns equals the parity as modified by removing the other columns.
             * M[4,4] 
             */
            m.m00 = Constants.p_constants[selected_indexes[0]];
            m.m01 = Constants.p_constants[selected_indexes[1]];
            m.m02 = Constants.p_constants[selected_indexes[2]];
            m.m03 = Constants.p_constants[selected_indexes[3]];

            m.m10 = Constants.q_constants[selected_indexes[0]];
            m.m11 = Constants.q_constants[selected_indexes[1]];
            m.m12 = Constants.q_constants[selected_indexes[2]];
            m.m13 = Constants.q_constants[selected_indexes[3]];

            m.m20 = Constants.r_constants[selected_indexes[0]];
            m.m21 = Constants.r_constants[selected_indexes[1]];
            m.m22 = Constants.r_constants[selected_indexes[2]];
            m.m23 = Constants.r_constants[selected_indexes[3]];

            m.m30 = Constants.s_constants[selected_indexes[0]];
            m.m31 = Constants.s_constants[selected_indexes[1]];
            m.m32 = Constants.s_constants[selected_indexes[2]];
            m.m33 = Constants.s_constants[selected_indexes[3]];

            return m;
        }

    }
}
