﻿/*  SharpBitRotRepair computes parity information, and if the original file is corrupted can try to repair it.
    Copyright (C) 2022 Robert Denier

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
using SharpBitRotChecker.Configuration;
using SharpBitRotChecker.Reader;
using System;

namespace SharpBitRotRepair.Algorithm
{
    public class CorruptFile : ParseFile
    {
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();
        private readonly Int64[] Shard = new Int64[Constants.WordsPerShard];

        private CorruptFile(string sourceFile, string mapName = "ImgA")
            : base(sourceFile, true /* allow write */)
        {
            log.Info("Corrupting <" + sourceFile + ">...");
        }


        /// <summary>
        /// The idea behind this is to be able to carefully control where errors are introduced for the purposes of testing.
        /// No attempt is made to make this process efficient, since it is only for testing.
        /// </summary>
        /// <param name="chunk"></param>
        /// <param name="shard"></param>
        /// <param name="offset"></param>
        /// <param name="newValue"></param>
        /// <returns></returns>
        private bool CorruptTheFile(int chunk, int corruptionsPerShard, int numShards)
        {
            log.Info("Using chunk='" + chunk + "'.  Corrupting " + numShards + " shards.  There will be " + corruptionsPerShard + "' curruptions per shard.");
            base.SetRegion(chunk);
            //Get the shard
            Random random = new Random(Environment.TickCount);
            for (int shard = 0; shard < numShards; shard++)
            {
                base.GetShard(shard, this.Shard);

                for (int i = 0; i < corruptionsPerShard; i++)
                {
                    //Select at random.  This may corrupt the same value twice.
                    //This should be adequate given the tests done.
                    int rnd = (int)Math.Round(31.1 * random.NextDouble());
                    if (rnd > 31) rnd = 31;
                    //Setting the presumably wrong value
                    //These are originally 32 bit, so this should usually work.
                    this.Shard[rnd] = 0XBADBEEF + rnd;
                }
                //Set the shard
                base.SetShard(shard, this.Shard);
            }
            base.UnsetRegion();
            return true;
        }

        public static void ProcessFile(string sourceFile, int chunk, int corruptionsPerShard, int numShards)
        {
            CorruptFile create = new CorruptFile(sourceFile);
            create.CorruptTheFile(chunk, corruptionsPerShard, numShards);
            create.Dispose();
        }




    }
}
