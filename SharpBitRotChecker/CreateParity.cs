﻿/*  SharpBitRotRepair computes parity information, and if the original file is corrupted can try to repair it.
    Copyright (C) 2022 Robert Denier

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
using SharpBitRotChecker.Algorithm;
using SharpBitRotChecker.Reader;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;

namespace SharpBitRotChecker
{
    /// <summary>
    /// This creates the parity file for the associated data file using this solutions parity format.
    /// </summary>
    public class CreateParity : ParseFile
    {
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();
        public ParseFile parseFile;
        private BinaryWriter parityWriter;

        private CreateParity(string file, string parityFile, bool overwrite): base(file, false /* allow write */)
        {
            log.Info("Creating <" + parityFile + "> for <" + file + ">...");
            string folder = Directory.GetParent(parityFile).FullName;
            if (!Directory.Exists(folder))
            {
                log.Warn("Creating <" + folder + ">...");
                Directory.CreateDirectory(folder);
            }
            if (overwrite || !File.Exists(parityFile))
            {
                this.parityWriter = new BinaryWriter(File.Create(parityFile));
            } else
            {
                log.Warn("<" + parityFile + "> already exists.");
                this.configured = false;
            }
        }

        /// <summary>
        /// This is going to write the parity chunk tp a file.
        /// </summary>
        /// <returns>True if the write succeded.</returns>
        private bool WriteParityChunk()
        {
            //compressed should be allocated for the worst case.
            //compressed size will be what is written.
            WorkingSetCompressed.GetCompressedWorkingSets(WorkingSets, compressed, this.settings.NumShards);
            int compressed_size = this.settings.NumShards * 3;
            //4/15/2022 - Verified the number of bytes is 8 times the compressed array size.
            Span<byte> bytes = MemoryMarshal.Cast<UInt64, byte>(compressed.AsSpan(0, compressed_size));
            this.parityWriter.Write(bytes);
            return true;
        }

        private void CreateTheParity(string file)
        {
            base.ResetTimer();
            for (Int32 chunk = 0; chunk <= lastChunk; chunk++)
            {
                base.tmpErrors.Clear();

                base.SetRegion(chunk);
                base.FillInData();

                //For now this is assumed to succeed.
                //later it may update errors.
                this.WriteParityChunk();

                base.UnsetRegion();
                base.LogNowAndThen(file, chunk);

                if (tmpErrors.Count > 0)
                {
                    log.Error("'" + tmpErrors.Count + "' errors detected in <" + file + ">.");
                    base.ParseFileErrors.AddRange(tmpErrors);
                }
            }
            this.Dispose();
        }


        public static List<ParseFileError> ProcessFile(string file, string parity_file, bool overwrite)
        {
            CreateParity create = new CreateParity(file, parity_file,overwrite);
            if (!create.configured)
            {
                create.Dispose();
                return null;
            }
            create.CreateTheParity(file);
            return create.ParseFileErrors;
        }

        /// <summary>
        /// Dispose of anything open...
        /// </summary>
        new private void Dispose()
        {
            base.Dispose();
            if (this.parityWriter != null)
            {
                this.parityWriter.Flush();
                this.parityWriter.Close();
                this.parityWriter.Dispose();
                this.parityWriter = null;
            }
        }
    }
}
