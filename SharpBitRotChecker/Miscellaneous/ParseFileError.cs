﻿/*  SharpBitRotRepair computes parity information, and if the original file is corrupted can try to repair it.
    Copyright (C) 2022 Robert Denier

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
using System;

namespace SharpBitRotChecker.Reader
{
    /// <summary>
    /// When an error occurs, this is the limit of what is immediately knowable.
    /// </summary>
    public struct ParseFileError
    {
        /// <summary>
        /// Which file chunks.  Chunks are 8MB sections of files.
        /// </summary>
        public Int32 chunk;
        /// <summary>
        /// Which shard.  Shards are interlaced to detect and correct more burst errors.
        /// </summary>
        public Int32 shard;
        /// <summary>
        /// This is the number of shards that were available with this error.
        /// </summary>
        public Int32 numShards;

        public ParseFileError(Int32 chunk, Int32 shard, Int32 numShards)
        {
            this.chunk = chunk;
            this.shard = shard;
            this.numShards = numShards;
        }

        public override string ToString()
        {
            return "Chunk=" + chunk + " Shard=" + shard;
        }
    }
}
