﻿/*  SharpBitRotRepair computes parity information, and if the original file is corrupted can try to repair it.
    Copyright (C) 2022 Robert Denier

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Runtime.CompilerServices;
using RWD_Common.Files;
using RWD_Common.Math;
using RWD_Common.Misc.File_Utils;
using SharpBitRotChecker.Configuration;

namespace SharpBitRotChecker.Reader
{
    public class ParseFile
    {
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();
        protected Int32 lastChunk;
        protected Settings settings = new Settings();
        protected List<ParseFileError> tmpErrors = new List<ParseFileError>();
        /// <summary>
        /// NOTE:  Make sure only the actual number are recorded/used.
        /// </summary>
        protected Column4x1[] WorkingSets = new Column4x1[Constants.MaxNumShards];
        protected UInt64[] compressed = new UInt64[Constants.MaxNumShards * 3];
        protected List<ParseFileError> ParseFileErrors = new List<ParseFileError>();
        private Int64 ticks;
        private readonly Int64 firstInvalidByte;
        private readonly MemoryMappedFileAccess fileAccess;
        private readonly MemoryMappedFile mmf;
        private MemoryMappedViewAccessor accessor;
        private Int64 fileSize;
        private bool isLastChunk = false;
        protected bool configured = false;


        //For possibly faster access
        private int BytesPerRead;
        private int NumShards;

        public ParseFile(string filename, bool allowWrite)
        {
            if (!File.Exists(filename))
            {
                log.Error("<" + filename + "> does not exist.");
                return;
            }
            this.ParseFileErrors.Clear();
            if (allowWrite)
            {
                this.mmf = MemMapUtility.GetReadWriteFile(filename);
            }
            else
            {
                this.mmf = MemMapUtility.GetReadOnlyFile(filename);
            }
            if (allowWrite)
            {
                fileAccess = MemoryMappedFileAccess.ReadWrite;
            }else
            {
                fileAccess = MemoryMappedFileAccess.Read;
            }

            this.accessor = mmf.CreateViewAccessor(0, 0, this.fileAccess);
            this.fileSize = FileUtils.GetFileSize(filename, true /* skip check exists. */);
            this.lastChunk = (Int32)(this.fileSize / Constants.MaxBytesPerRead);
            if (this.lastChunk*Constants.MaxBytesPerRead == this.fileSize)
            {
                this.lastChunk--;
            }
            this.firstInvalidByte = (this.fileSize % Constants.MaxBytesPerRead);
            //If this is an exact multiple...
            if (this.firstInvalidByte == 0)
            {
                this.firstInvalidByte = Constants.MaxBytesPerRead;
            }


            //Question: What about ending at something other than a multiple of 4 bytes?
            //Answer: Up to 3 bytes at end will not be protected, since they will always be "read" as 0x00000000;
            //Note: May want to explicitly not try to repair those possible last 3.
            this.accessor.Dispose();
            this.configured = true;
        }

        protected void ResetTimer()
        {
            this.ticks = Environment.TickCount64;
        }

        protected void LogNowAndThen(string file, int chunk)
        {
            var delta = Environment.TickCount64 - this.ticks;
            if (delta > 10000)
            {
                ResetTimer();
                log.Info("Processing chunk " + chunk + ".");
            }
        }

        protected void LogNowAndThen(string file, int chunk, int shard)
        {
            var delta = Environment.TickCount64 - this.ticks;
            if (delta > 2000)
            {
                ResetTimer();
                log.Info("Processing " + chunk + "." + shard + " (chunk.shard)");
            }
        }

        protected void SetRegion(Int64 chunk)
        {
            var length = Constants.MaxBytesPerRead;
            if (chunk == this.lastChunk)
            {
                length = 0;
                this.isLastChunk = true;
            }
            Int64 offset = (chunk * Constants.MaxBytesPerRead);
            Int64 len_remaining = this.fileSize - offset;
            this.settings.UpdateSettings(len_remaining);
            this.BytesPerRead = this.settings.BytesPerRead;
            this.NumShards = this.settings.NumShards;
            this.accessor = mmf.CreateViewAccessor(offset, length, this.fileAccess);
            for(int i = 0; i < settings.NumShards; i++)
            {
                this.WorkingSets[i].Clear();
            }
        }
        public void UnsetRegion()
        {
            this.accessor.Dispose();
        }


        /// <summary>
        /// Reads 4 bytes.
        /// This is designed to read past the valid data and just return 0's.
        /// The reason for this is to prevent special case handling.  The 0's returned will not affect the sum in any event.
        /// During any reconstruction, these fake values should/must always be assumed correct since they are always 0.
        /// </summary>
        /// <param name="position">This is a byte offset from the current section.</param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected Int64 Read(Int64 position)
        {
            if (this.isLastChunk && position >= this.firstInvalidByte)
            {
                return 0;
            }
            return this.accessor.ReadUInt32(position);
        }


        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected void Write(Int64 position, Int64 data)
        {
            if (this.isLastChunk && position >= this.firstInvalidByte)
            {
                return;
            }
            this.accessor.Write(position, (UInt32)data);
        }

        /// <summary>
        /// This is going to touch each working set 32 times.
        /// The final value here will be the working set state which contains the actual value of p,q,r, and s for each set.
        /// This can then be compared with saved values or saved.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected void FillInData()
        {
            int shard = 0;
            int word = 0;
            for(Int64 position = 0; position < this.BytesPerRead; position+=4)
            {
                Int64 x = this.Read(position);//x = 0xFFFFFFFF; //set this to the max value for finding numeric limits.
                this.WorkingSets[shard].p += Constants.p_constants[word] * x;
                this.WorkingSets[shard].q += Constants.q_constants[word] * x;
                this.WorkingSets[shard].r += Constants.r_constants[word] * x;
                this.WorkingSets[shard].s += Constants.s_constants[word] * x;
                shard++;
                if (shard == this.NumShards)
                {
                    word++;
                    shard = 0;
                }
            }
        }



        #region FunctionsUsedToRepair
        /// <summary>
        /// This collects and places the data that is known to be bad in a single Int64 array.
        /// This can be used as a step in the repair process.
        /// It is here, because it fits better with data parsing, than the repair functionality.
        /// The repair functionality will take this and use the parity data to repair it.
        /// </summary>
        public void GetShard(int shard_to_get, Int64 [] shard)
        {
            int shard_cnt = 0;
            int word = 0;
            for (Int64 position = 0; position < this.BytesPerRead; position += 4)
            {
                //Every step advances to the next shard.
                //Until we get back to where we started (or run out).
                //Every time we get back it updates the shard array.
                if (shard_to_get == shard_cnt)
                {
                    Int64 x = this.Read(position);
                    shard[word++] = x;
                }
                shard_cnt++;
                if (shard_cnt == this.NumShards)
                {
                    shard_cnt = 0;
                }
            }
        }

        public void SetShard(int shard_to_set, Int64 [] shard)
        {
            int shard_cnt = 0;
            int word = 0;
            for (Int64 position = 0; position < this.BytesPerRead; position += 4)
            {
                //Every step advances to the next shard.
                //Until we get back to where we started (or run out).
                //Every time we get back it updates the shard array.
                if (shard_to_set == shard_cnt)
                {
                    this.Write(position, shard[word++]);
                }
                shard_cnt++;
                if (shard_cnt == this.NumShards)
                {
                    shard_cnt = 0;
                }
            }
        }

        #endregion
        protected void Dispose()
        {
            this.mmf.Dispose();
        }
    }
}
