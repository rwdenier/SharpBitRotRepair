﻿/*  SharpBitRotRepair computes parity information, and if the original file is corrupted can try to repair it.
    Copyright (C) 2022 Robert Denier

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
using RWD_Common.Math;
using System;

namespace SharpBitRotChecker.Algorithm
{
    /// <summary>
    /// This should work to compress any valid WorkingSet.  8 bytes are removed.
    /// Since this is just removing bytes, the hext digits should all still be there in the same order.
    /// </summary>
    public struct WorkingSetCompressed
    {
        public UInt64 w1;
        public UInt64 w2;
        public UInt64 w3;

        public WorkingSetCompressed(UInt64 w1, UInt64 w2, UInt64 w3)
        {
            this.w1 = w1;
            this.w2 = w2;
            this.w3 = w3;
        }

        public WorkingSetCompressed(Column4x1 workingSet)
        {
            //p_sum;//MAX: 00 00 00 [1F FF FF FF E0]
            //q_sum;//MAX: 00 00 [02 0F FF] FF FD F0
            this.w1 = ((((UInt64)workingSet.p) & 0xFFFFFFFFFF) << 24) + (((UInt64)workingSet.q)  >> 24);
            //q_sum;//MAX: 00 00  02 0F FF [FF FD F0]
            //r_sum;//MAX: 00 00 [2C AF FF  FF D3] 50
            this.w2 = ((((UInt64)workingSet.q) & 0xFFFFFF) << 40) + ((((UInt64)workingSet.r) & 0xFFFFFFFFFF00) >> 8);
            //r_sum;//MAX: 00  00 2C AF FF FF D3 [50]
            //s_sum;//MAX: 00 [04 40 FF FF FB BF  00] 
            this.w3 = ((((UInt64)workingSet.r) & 0xFF) << 56) + (UInt64)workingSet.s;

            //[       w1                ][            w2           ][             w3           ] 
            //[1F FF FF FF E0 | 02 0F FF][FF FD F0 | 2C AF FF FF D3][50 | 04 40 FF FF FB BF 00 ]
            //[      P        |          Q         |        R           |          S           ]
            // 
        }

        public Column4x1 GetWorkingSet()
        {
            Column4x1 x;
            x.valid = true;
            x.p = (Int64)(this.w1 >> 24);
            x.q = (Int64)(((this.w1 & 0xFFFFFF) << 24) + (this.w2 >> 40));
            x.r = (Int64)(((this.w2 & 0xFFFFFFFFFF) << 8) + (this.w3 >> 56));
            x.s = (Int64)(this.w3 & 0xFFFFFFFFFFFFFF);
            return x;
        }

        /// <summary>
        /// Each compressed working set takes 3 UInt64's.
        /// </summary>
        /// <param name="WorkingSets"> has length of Constants.NumShards</param>
        /// <param name="compressed"></param>
        /// <param name="num_shards"></param>
        public static void GetCompressedWorkingSets(Column4x1[] WorkingSets, UInt64[] compressed, int num_shards)
        {
            for(int i = 0; i < num_shards; i++)
            {
                WorkingSetCompressed wsc = new WorkingSetCompressed(WorkingSets[i]);
                compressed[3 * i + 0] = wsc.w1;
                compressed[3 * i + 1] = wsc.w2;
                compressed[3 * i + 2] = wsc.w3;
            }
        }
    }
}
