﻿/*  SharpBitRotRepair computes parity information, and if the original file is corrupted can try to repair it.
    Copyright (C) 2022 Robert Denier

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
using System.Collections.Generic;

namespace SharpBitRotChecker.Reader
{
    /// <summary>
    /// This keeps track of what went wrong, so the repair portion can try to fix it.
    /// </summary>
    public class ErrorInfo
    {
        public string SourceFile;
        public string ParityFile;
        public List<ParseFileError> errors;

        public ErrorInfo(string source_file, string parity_file, List<ParseFileError> errors)
        {
            this.SourceFile = source_file;
            this.ParityFile = parity_file;
            this.errors = errors;
        }

        public override string ToString()
        {
            string str = this.SourceFile + ": ";
            foreach (var v in errors)
            {
                str += v.chunk + "." + v.shard + ", ";
            }
            return str;
        }
    }
}
