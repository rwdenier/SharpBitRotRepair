﻿/*  SharpBitRotRepair computes parity information, and if the original file is corrupted can try to repair it.
    Copyright (C) 2022 Robert Denier

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
using System.IO;

namespace SharpBitRotChecker.Miscellaneous
{
    public static class ExtensionMethods
    {
        /// <summary>
        /// This creates the parity file name from the old file name.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string ToParityFileName(this string path)
        {
            string folder = Directory.GetParent(path).FullName;
            string fileName = Path.GetFileNameWithoutExtension(path);
            string ext = Path.GetExtension(path).Trim('.');
            string newFileName = Path.Combine(folder, fileName + "_" + ext + ".bin");
            return newFileName;
        }
    }
}
