﻿/*  SharpBitRotRepair computes parity information, and if the original file is corrupted can try to repair it.
    Copyright (C) 2022 Robert Denier

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
using SharpBitRotChecker.Algorithm;
using SharpBitRotChecker.Reader;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;

namespace SharpBitRotChecker
{
    /// <summary>
    /// The CheckParity class is designed to take a file plus parity file pair and determine if there are problems and where they are.
    /// </summary>
    public class CheckParity : ParseFile
    {
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();
        private BinaryReader parityReader;
        private CheckParity(string file, string parityFile) : base(file, false /* allow write */)
        {
            log.Info("Checking <" + file + ">...");
            if (!File.Exists(parityFile))
            {
                log.Error("<" + parityFile + "> does not exist.");
                return;
            }
        }

        /// <summary>
        /// The goal of this is simply to check if things match.  
        /// If they don't they can be stored into a file which will allow
        /// the end user to determine how to proceed.  Repairing the file
        /// is expected to be slower, and while only work within the limit
        /// of the design.
        /// 
        /// This should abort scanning of the file once a failure is detected.
        /// It is worth noting that the first action an end user may take
        /// is to backup everything else, though the default action will
        /// be to continue to scan the other files.
        /// </summary>
        private void CheckParityChunk(Int32 chunk, List<ParseFileError> tmpErrors)
        {
            WorkingSetCompressed.GetCompressedWorkingSets(WorkingSets, compressed /*uint64 array*/, this.settings.NumShards);
            //TODO:  There is an allocation here that could probably be avoided.
            int compressed_size = this.settings.NumShards * 3; //size of array, so max element is this -1.
            var bytes2 = this.parityReader.ReadBytes(compressed_size * 8);
            Span<UInt64> loaded = MemoryMarshal.Cast<byte, UInt64>(bytes2.AsSpan());
            if (loaded.Length < compressed_size)
            {
                log.Error("'" + loaded.Length + " compressed parity words read from file.  Expected '" +
                    compressed_size + "'.  This may indicate parity file corruption.");
                tmpErrors.Add(new ParseFileError(chunk, -1, this.settings.NumShards));
                return;
            }

            Int32 lastShardError = -1;
            for (int i = 0; i < compressed_size; i++)
            {
                if (compressed[i] != loaded[i])
                {
                    //This comes from it taking 3 uint64 for a compressed working set.
                    Int32 shard = i / 3;
                    if (shard != lastShardError)
                    {
                        ParseFileError parseFileError = new ParseFileError(chunk, shard, this.settings.NumShards);
                        tmpErrors.Add(parseFileError);
                    }
                    lastShardError = shard;
                }
            }
        }

        /// <summary>
        /// Open the parity file, or report an error if it doesn't exist.
        /// </summary>
        /// <param name="parityFile"></param>
        /// <returns></returns>
        private bool OpenParityFile(string parityFile)
        {
            if (File.Exists(parityFile))
            {
                this.parityReader = new BinaryReader(File.OpenRead(parityFile));
                return true;
            }
            else
            {
                log.Error(parityFile + " does not exist.  Nothing to do...");
                return false;
            }
        }

        /// <summary>
        /// Need to figure out errors and such more..
        /// </summary>
        /// <param name="file"></param>
        /// <param name="parityFile"></param>
        private void DoTheCheck(string file, string parityFile)
        {
            if (!OpenParityFile(parityFile))
            {
                return;
            }
            base.ResetTimer();
            for (Int32 chunk = 0; chunk <= lastChunk; chunk++)
            {
                //tmpErrors was originally made separate as possibly work towards running this in multiple threads.
                //This is not implemented, but the idea would be for the thread to operate on
                //independent information and merge any errors upon completion.
                this.tmpErrors.Clear();
                base.SetRegion(chunk);
                base.FillInData();
                this.CheckParityChunk(chunk, tmpErrors);
                base.UnsetRegion();
                base.LogNowAndThen(file, chunk);
                if (tmpErrors.Count > 0)
                {
                    log.Error("'" + tmpErrors.Count + "' errors detected in <" + file + ">.");
                    base.ParseFileErrors.AddRange(tmpErrors);
                }
            }
            this.Dispose();
        }

        /// <summary>
        /// This does the actual work to check file parity.
        /// </summary>
        /// <param name="file"></param>
        /// <param name="parityFile"></param>
        /// <returns></returns>
        public static List<ParseFileError> ProcessFile(string file, string parityFile)
        {
            CheckParity check = new CheckParity(file, parityFile);
            if (!check.configured)
            {
                return null;
            }
            check.DoTheCheck(file,parityFile);
            return check.ParseFileErrors;
        }

        /// <summary>
        /// Dispose of what is open.
        /// </summary>
        new private void Dispose()
        {
            base.Dispose();
            if (this.parityReader != null)
            {
                this.parityReader.Close();
                this.parityReader.Dispose();
                this.parityReader = null;
            }
        }
    }
}
