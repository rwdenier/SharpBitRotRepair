﻿/*  SharpBitRotRepair computes parity information, and if the original file is corrupted can try to repair it.
    Copyright (C) 2022 Robert Denier

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
using System;

namespace SharpBitRotChecker.Configuration
{
    public class ParityMatrixBuilder
    {
        public static string BuildMatrix()
        {
            string s = "";
            s += build_line("p_constants", 0);
            s += build_line("q_constants", 1);
            s += build_line("r_constants", 2);
            s += build_line("s_constants", 3);
            return s;
        }

        /// <summary>
        ///This is just something quick to generate coefficients of the form:
        ///1^0 2^0 3^0 4^0 5^0 6^0 ... 32^0 
        ///1^1 2^1 3^1 4^1 5^1 6^1 ... 32^1
        ///1^2 2^2 3^2 4^2 5^2 6^2 ... 32^2
        ///1^3 2^3 3^3 4^3 5^3 6^3 ... 32^3
        /// </summary>
        /// <param name="cname"></param>
        /// <returns></returns>
        private static string build_line(string cname, int exponent)
        {
            string s = "static readonly Int64[] " + cname + " = {";
            for(Int64 i = 1; i <= Constants.WordsPerShard; i++)
            {
                switch (exponent)
                {
                    case 0:
                        s += "1";
                        break;
                    case 1:
                        s += i.ToString(); 
                        break;
                    case 2:
                        s += (i*i).ToString();
                        break;
                    case 3:
                        s += (i*i*i).ToString();
                        break;
                }
                s += ",";
            }
            s = s.Substring(0,s.Length - 1);
            s += "};\r\n";
            return s;
        }
    }
}
