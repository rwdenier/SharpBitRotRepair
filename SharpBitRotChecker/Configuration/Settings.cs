﻿/*  SharpBitRotRepair computes parity information, and if the original file is corrupted can try to repair it.
    Copyright (C) 2022 Robert Denier

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
using System;

namespace SharpBitRotChecker.Configuration
{
    /// <summary>
    /// This calculates the number of shards, which is a constant, except for the last shard.
    /// </summary>
    public class Settings
    {
        public int NumShards;
        public int WordsPerRead;
        public int BytesPerRead;

        public Settings()
        {
        }
        
        /// <summary>
        /// Updates NumShards, WordsPerRead, and BytesPerRead if necessary.
        /// </summary>
        /// <param name="bytes_remaining"></param>
        public void UpdateSettings(Int64 bytes_remaining)
        {
            if (bytes_remaining >= Constants.MaxBytesPerRead)
            {
                this.NumShards = Constants.MaxNumShards;
            }
            else
            {
                double x = (((double)bytes_remaining) / Constants.ShardSizeBytes);
                x = Math.Ceiling(x);
                this.NumShards = (int)x;
            }
            this.WordsPerRead = this.NumShards * Constants.WordsPerShard;
            this.BytesPerRead = this.NumShards * Constants.ShardSizeBytes;
        }
    }
}
