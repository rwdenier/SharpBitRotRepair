﻿/*  SharpBitRotRepair computes parity information, and if the original file is corrupted can try to repair it.
    Copyright (C) 2022 Robert Denier

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
using System;

namespace SharpBitRotChecker.Configuration
{
    public class Constants
    {
        public const int ShardSizeBytes = 128;
        public const int WordsPerShard = 32;
        /// <summary>
        /// This is going to have to be set per file, with the upper bound being 65536, probably.
        /// Otherwise, a tiny file will have an over 1MB parity file.
        /// </summary>
        public const int MaxNumShards = 65536;
        public const int MaxWordsPerRead = MaxNumShards * WordsPerShard;

        /// <summary>
        /// 8 MB per Chunk
        /// </summary>
        public const int MaxBytesPerRead = MaxNumShards * ShardSizeBytes;

        #region Parity Matrix Constants
        //Generated with Parity Matrix Builder
        public static readonly Int64[] p_constants = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
        public static readonly Int64[] q_constants = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32 };
        public static readonly Int64[] r_constants = { 1, 4, 9, 16, 25, 36, 49, 64, 81, 100, 121, 144, 169, 196, 225, 256, 289, 324, 361, 400, 441, 484, 529, 576, 625, 676, 729, 784, 841, 900, 961, 1024 };
        public static readonly Int64[] s_constants = { 1, 8, 27, 64, 125, 216, 343, 512, 729, 1000, 1331, 1728, 2197, 2744, 3375, 4096, 4913, 5832, 6859, 8000, 9261, 10648, 12167, 13824, 15625, 17576, 19683, 21952, 24389, 27000, 29791, 32768 };
        #endregion;

        #region Max Values
        //These are based on just analyzing the worst case.
        public const Int64 p_sum_max =     0x1FFFFFFFE0;
        public const Int64 q_sum_max =   0x020FFFFFFDF0;
        public const Int64 r_sum_max =   0x2CAFFFFFD350;
        public const Int64 s_sum_max = 0x0440FFFFFBBF00;
        #endregion
    }
}
