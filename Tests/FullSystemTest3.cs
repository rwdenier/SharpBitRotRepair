﻿/*  SharpBitRotRepair computes parity information, and if the original file is corrupted can try to repair it.
    Copyright (C) 2022 Robert Denier

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
using Xunit;
using static Tests.FullSystemTestHelper;

namespace Tests
{
    public class FullSystemTest3
    {
        public FullSystemTest3()
        {
            Debugging.Instance.EnableLogging();
        }

        [Fact]
        public void Chunk2of3()
        {
            FullSystemTestHelper h = new FullSystemTestHelper();
            h.CreateTestFile(FillPattern.Mod16, 2/*0 based chunk*/, 3.2 /* chunks*/);
        }
    }
}
