/*  SharpBitRotRepair computes parity information, and if the original file is corrupted can try to repair it.
    Copyright (C) 2022 Robert Denier

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
using RWD_Common.Math;
using SharpBitRotChecker.Algorithm;
using SharpBitRotChecker.Configuration;
using System;
using Xunit;

namespace Tests
{
    /// <summary>
    /// This is just making sure my 4 word to 3 word compression is reliable.
    /// The compression is just removing the space where zeros would be.
    /// </summary>
    public class WorkingSetTests
    {
        [Fact]
        public void CompressionDecompressionTest()
        {
            //This should be a decent test of the compressed working sets.
            //This should show that working sets can be compressed and uncompressed losslessly within the limits of this design.
            Column4x1 ws;
            ws.valid = true;
            const int steps = 20;
            for (Int64 p = 0; p < Constants.p_sum_max; p += Constants.p_sum_max/steps)
            {
                for (Int64 q = 0; q < Constants.q_sum_max; q += Constants.q_sum_max / steps)
                {
                    for (Int64 r = 0; r < Constants.r_sum_max; r += Constants.r_sum_max / steps)
                    {
                        for (Int64 s = 0; s < Constants.s_sum_max; s += Constants.s_sum_max / steps)
                        {
                            ws.p = p;
                            ws.q = q;
                            ws.r = r;
                            ws.s = s;
                            WorkingSetCompressed wsc = new WorkingSetCompressed(ws);
                            Column4x1 ws2 = wsc.GetWorkingSet();
                            //If all tests are good, then there is no need to go into the asserts.
                            if (!ws.Equals(ws2))
                            {
                                Assert.True((ws.p == ws2.p), "p_sum != p_sum' " + ws.p.ToString("X") + "!=" + ws2.p.ToString("X") + " w1=" + wsc.w1.ToString("X"));
                                Assert.True((ws.q == ws2.q), "q_sum != q_sum' " + ws.q.ToString("X") + "!=" + ws2.q.ToString("X") + " w1=" + wsc.w1.ToString("X") + " w2=" + wsc.w2.ToString("X"));
                                Assert.True((ws.r == ws2.r), "r_sum != r_sum' " + ws.r.ToString("X") + "!=" + ws2.r.ToString("X") + " w2=" + wsc.w2.ToString("X") + " w3=" + wsc.w3.ToString("X"));
                                Assert.True((ws.s == ws2.s), "s_sum != s_sum' " + ws.s.ToString("X") + "!=" + ws2.s.ToString("X") + " w3=" + wsc.w3.ToString("X"));
                            }
                        }
                    }
                }
            }
        }    
    }
}
