/*  SharpBitRotRepair computes parity information, and if the original file is corrupted can try to repair it.
    Copyright (C) 2022 Robert Denier

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
using RWD_Common.Math;
using System;
using Xunit;

namespace Tests
{
    public class Matrix4x4Tests
    {
        [Fact]
        public void IdentityTests()
        {
            Matrix4x4 m4 = new Matrix4x4();

            m4.CreateIdentityMatrix();
            m4.CalcDeterminate();
            Assert.True((1/m4.inverseDet == 1), "det != 1'");

            Column4x1 column4X1 = new Column4x1(2, 3, 4, 5);
            Column4x1 c2 = m4.Multiply(column4X1);
            Assert.True((c2.Equals(column4X1)), "c2 != column4x1'");
        }

        [Fact]
        public void RegularMultiplication() 
        { 
            //https://matrix.reshish.com/multCalculation.php
            Matrix4x4 m = new Matrix4x4(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16);
            Column4x1 c = new Column4x1(5, 6, 7, 8);
            Column4x1 cm = m.Multiply(c);
            Column4x1 ans = new Column4x1(70,174,278,382);
            Assert.True((cm.Equals(ans)), "c != ans'");
        }


        [Fact]
        public void TestingInversion()
        {
            //An invertible matrix
            //Result and problem pulled from
            //https://cosmolearning.org/video-lectures/inverse-4x4-matrix-using-adjugate-formula/
            Matrix4x4 m = new Matrix4x4(1, 0, 0, 1, 0, 2, 1, 2, 2, 1, 0, 1, 2, 0, 1, 4);
            Matrix4x4 mInv = m.PartialInverse();
            //The inverse above is incomplete, since the general case for this work must be lossless.
            //ToDecimalString finishes it.
            string s = mInv.ToDecimalString();
            string ans = "[-2, -0.5, 1, 0.5]\r\n";
            ans += "[1, 0.5, 0, -0.5]\r\n";
            ans += "[-8, -1, 2, 2]\r\n";
            ans += "[3, 0.5, -1, -0.5]";
            Assert.True((s.Equals(ans)), "s != ans'");
        }

        /// <summary>
        /// The general case is
        /// 1^0 + 2^0 + 3^0 + 4^0 ... = 1,1,1,1 ...
        /// 1^1 + 2^1 + 3^1 + 4^1 ... = 1,2,3,4 ...
        /// 1^2 + 2^2 + 3^2 + 4^2 ... = 1,4,9,16 ...
        /// 
        /// x[0] +  x[1] +  x[2] +  x[3] ... = p[0]  //If you know which is bad, you can fix it with 1
        /// x[0] + 2x[1] + 3x[2] + 4x[3] ... = p[1] 
        /// x[0] + 4x[1] + 9x[2] + 16x[3] ...= p[2] 
        /// So in general, perfect parity can fix 4 values in this example, if you can find the 4 values.
        /// 
        /// The general case of repairing a file is to replace 4 values and see if the equation solves.
        /// So you go from M[4,32] to M[4,4] 
        /// M[4,4] x V[4] = PV[4]  (PV is the new parity vector.)
        /// V is the unknown, so V = Minverse * PV
        /// </summary>
        [Fact]
        public void LinearSystemSolver()
        {
            //Creating a 4x4 matrix out of this means multiplying by
            //1,1,1,1
            //1,2,3,4
            //1,4,9,16
            //1,8,27,64
            Matrix4x4 m = new Matrix4x4(
                1 * 1, 1 * 2, 1 * 3, 1 * 4,
                1 * 1, 2 * 2, 3 * 3, 4 * 4,
                1 * 1, 4 * 2, 9 * 3, 16 * 4,
                1 * 1, 8 * 2, 27 * 3, 64 * 4);

            Matrix4x4 m2 = new Matrix4x4(
               1, 1, 1, 1,
               1, 2, 3, 4,
               1, 4, 9, 16,
               1, 8, 27, 64);

            Column4x1 data = new Column4x1(1,2,3,4);

            Column4x1 parity = new Column4x1(
                m.m00 + m.m01 + m.m02 + m.m03,
                m.m10 + m.m11 + m.m12 + m.m13,
                m.m20 + m.m21 + m.m22 + m.m23,
                m.m30 + m.m31 + m.m32 + m.m33);

            Matrix4x4 mInv= m.PartialInverse();
            Column4x1 x = mInv.Multiply(parity);//all 1s
            Matrix4x4 m2Inv = m2.PartialInverse();
            Column4x1 original_values_reconstructed = m2Inv.Multiply(parity);

            TestLinearSystem(999, 33, 1231, 5234);
        }


        private void TestLinearSystem(Int64 d0, Int64 d1, Int64 d2, Int64 d3)
        {
            Column4x1 data = new Column4x1(d0,d1,d2,d3);
            Matrix4x4 m = new Matrix4x4(
               1, 1, 1, 1,
               1, 2, 3, 4,
               1, 4, 9, 16,
               1, 8, 27, 64);
            Column4x1 parity = m.Multiply(data);
            //The inverse is using coefficients not data.
            Matrix4x4 mInv = m.PartialInverse();
            Column4x1 reconstructed = mInv.Multiply(parity);
            Assert.True(data.Equals(reconstructed), "Expected reconstructed values to match.");
        }

    }
}
