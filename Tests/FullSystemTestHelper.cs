﻿/*  SharpBitRotRepair computes parity information, and if the original file is corrupted can try to repair it.
    Copyright (C) 2022 Robert Denier

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
using SharpBitRotChecker;
using SharpBitRotChecker.Configuration;
using SharpBitRotChecker.Reader;
using SharpBitRotRepair.Algorithm;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using Xunit;

namespace Tests
{
    internal class FullSystemTestHelper
    {
        internal enum FillPattern
        {
            Ones,
            Mod16,
            Mod2063,
            Random
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fillPattern"></param>
        /// <param name="chunk"></param>
        /// <param name="numChunks">8MB chunks</param>
        public void CreateTestFile(FillPattern fillPattern, int chunk, double numChunks)
        {
            Int64 sizeInBytes = (Int64)(Constants.MaxBytesPerRead * numChunks);

            string filename = Path.GetTempFileName();
            string parity_file = Path.GetTempFileName();
            string corrupted = Path.GetTempFileName();
            //This is the original data that is kept for testing.
            byte[] originalBuffer = CreateBuffer(fillPattern, sizeInBytes);

            File.WriteAllBytes(filename, originalBuffer);
            CreateParity.ProcessFile(filename, parity_file, true /*overwrite*/);

            List<ParseFileError> list = CheckParity.ProcessFile(filename, parity_file);
            CorruptFile.ProcessFile(filename, chunk /*chunk*/, 3 /* corruptionsPerShard*/, 650 /*numShards*/);

            List<ParseFileError> list2 = CheckParity.ProcessFile(filename, parity_file);
            Assert.True(list2.Count > 0, "File should have had errors.");
            if (list2.Count > 0)
            {
                byte[] badBytes = File.ReadAllBytes(filename);
                Assert.False(Array.Equals(originalBuffer, badBytes), "File is not corrupted when it should be.");
                int badBytesIdx = FindMismatch(originalBuffer, badBytes);

                ErrorInfo errorInfo = new ErrorInfo(filename, parity_file, list2);
                RepairFile.ProcessFile(errorInfo);

                byte[] goodBytes = File.ReadAllBytes(filename);
                Assert.True(originalBuffer.Length == goodBytes.Length, "Final file should be the same size.");
                int idx = FindMismatch(originalBuffer, goodBytes);
                Assert.True(idx == originalBuffer.Length, "File is corrupted when it should be repaired.  First mismatch=" + idx + "(Corrupted version was " + badBytesIdx);
            }

            List<ParseFileError> lastCheck = CheckParity.ProcessFile(filename, parity_file);
            Assert.True(lastCheck.Count == 0, "Expected no errors in the final test.");

            File.Delete(filename);
            File.Delete(parity_file);
        }

        private int FindMismatch(byte[] a, byte[] b)
        {
            for (int i = 0; i < a.Length; i++)
            {
                if (a[i] != b[i])
                {
                    return i;
                }
            }
            return a.Length;
        }

        private byte[] CreateBuffer(FillPattern fill, Int64 sizeInBytes)
        {
            if (fill == FillPattern.Ones)
            {
                UInt32[] buffer = new UInt32[sizeInBytes / 4];
                Array.Fill<UInt32>(buffer, 1);
                return MemoryMarshal.Cast<UInt32, byte>(buffer).ToArray();
            }
            else if (fill == FillPattern.Random)
            {
                byte[] buffer = new byte[sizeInBytes];
                Random r = new Random();
                r.NextBytes(buffer);
                return buffer;
            } else
            {
                UInt32[] buffer = new UInt32[sizeInBytes / 4];
                UInt32 xMax = 256;
                if (fill == FillPattern.Mod2063) xMax = 2063;
                UInt32 x = 0;
                for (int i = 0; i < buffer.Length; i++)
                {
                    buffer[i] = x;
                    x++;
                    if (x == xMax) x = 0;
                }
                return MemoryMarshal.Cast<UInt32, byte>(buffer).ToArray();
            }
        }
    }
}
