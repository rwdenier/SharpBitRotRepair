﻿/*  SharpBitRotRepair computes parity information, and if the original file is corrupted can try to repair it.
    Copyright (C) 2022 Robert Denier

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
using SharpBitRotRepair.Algorithm;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Tests
{
    public class ShardRepairSetTests
    {
        /// <summary>
        /// This is fairly pendantic, and won't insure a complete set.
        /// It should, nevertheless detect most issues with selected and not selected sets.
        /// </summary>
        [Fact]
        public void CheckSelectedAndNotSelectedBasics()
        {
            HashSet<string> selected = new HashSet<string>();
            HashSet<string> notSelected = new HashSet<string>();
            foreach (var v in ShardRepairPaths.Sets)
            {
                HashSet<int> sel = new HashSet<int>();
                HashSet<int> notSel = new HashSet<int>();
                foreach (var v1 in v.SelectedIndexes)
                {
                    sel.Add(v1);
                    Assert.True(v1 >= 0, "Selected indices should be positive.");
                    Assert.True(v1 <= 31, "Selected indices should be <=31.");
                }
                foreach (var v1 in v.NotSelectedIndexes)
                {
                    Assert.False(sel.Contains(v1), "Not Selected Indices should not contain Selected Indices");
                    notSel.Add(v1);
                    Assert.True(v1 >= 0, "Not selected indices should be positive.");
                    Assert.True(v1 <= 31, "Not selected indices should be <=31.");
                }
                var selArray = sel.ToArray();
                var notSelArray = notSel.ToArray();
                Assert.True(selArray.Length == 4, "4 selected indices are required.");
                Assert.True(notSelArray.Length == 28, "28 not selected indices are required.");
                Array.Sort(selArray);
                Array.Sort(notSelArray);
                selected.Add(string.Join(',', selArray));
                notSelected.Add(string.Join(',', notSelArray));
            }
            Assert.True(selected.Count == ShardRepairPaths.Sets.Count, "The number of unique selected sets is less than the total.");
            Assert.True(notSelected.Count == ShardRepairPaths.Sets.Count, "The number of unique not selected sets is less than the total.");
        }
    }
}
