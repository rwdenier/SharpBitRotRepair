﻿/*  SharpBitRotRepair computes parity information, and if the original file is corrupted can try to repair it.
    Copyright (C) 2022 Robert Denier

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
using System.Windows;
using System.Windows.Media;

namespace RWD_Common.Misc.Constants
{
    public class CommonConstants
    {
        public static readonly Brush DefaultForegroundBrush = Brushes.GhostWhite;
        public static readonly Brush DefaultBackgroundBrush = new SolidColorBrush(Color.FromArgb(128, 150, 140, 140));
        public static readonly Thickness DefaultMargin = new Thickness(2);
        public static readonly Thickness DefaultBorderThickness = new Thickness(1);
        public static readonly double IconSize = 25;
    }
}
