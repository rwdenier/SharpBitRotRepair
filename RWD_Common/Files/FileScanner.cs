﻿/*  SharpBitRotRepair computes parity information, and if the original file is corrupted can try to repair it.
    Copyright (C) 2022 Robert Denier

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
using RWD_Common.Misc.File_Utils;
using System.IO;

namespace RWD_Common.Files
{
    /// <summary>
    /// The FileScanner class is designed to allow processing of files where there is a source and a target file.
    /// The actual operation can be specified with a delegate.
    /// </summary>
    public class FileScanner
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="options">Generic for now...</param>
        /// <param name="options2">Also generic for now...</param>
        /// <param name="source_file"></param>
        /// <param name="target_file"></param>
        public delegate void ScanDelegate(object options,object options2, string source_file, string target_file); // declare a delegate

        /// <summary>
        /// This is just a basic function to recurse a directory tree.
        /// No filtering is done, though it can easily be done in the delegate.
        /// </summary>
        /// <param name="options"></param>
        /// <param name="options2"></param>
        /// <param name="min_file_size"></param>
        /// <param name="source_folder_in"></param>
        /// <param name="target_folder_in"></param>
        /// <param name="del"></param>
        public static void Scan(object options,object options2, int min_file_size, string source_folder_in, string target_folder_in, ScanDelegate del)
        {
            var files = Directory.EnumerateFiles(source_folder_in);
            foreach (var file in files)
            {
                if (FileUtils.GetFileSize(file, false /* skip exist check*/) >= min_file_size)
                {
                    string name = Path.GetFileName(file);
                    string new_path = Path.Combine(target_folder_in, name);
                    del(options,options2, file, new_path);
                }
            }
            var folders = Directory.EnumerateDirectories(source_folder_in);
            foreach (var folder in folders)
            {
                string relative_path = Path.GetRelativePath(source_folder_in, folder);
                string new_target_folder = Path.Combine(target_folder_in, relative_path);
                Directory.CreateDirectory(new_target_folder);
                Scan(options,options2, min_file_size, folder,new_target_folder, del);
            }
        }

    }
}
