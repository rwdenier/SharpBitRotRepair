﻿/*  SharpBitRotRepair computes parity information, and if the original file is corrupted can try to repair it.
    Copyright (C) 2022 Robert Denier

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
using System.IO;
using System.IO.MemoryMappedFiles;

namespace RWD_Common.Files
{
    public static class MemMapUtility
    {
        /// <summary>
        /// https://stackoverflow.com/questions/24435260/how-to-have-shared-read-only-access-to-a-memory-mapped-file
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static MemoryMappedFile GetReadOnlyFile(string path)
        {
            return MemoryMappedFile.CreateFromFile(
                      //include a readonly shared stream
                      File.Open(path, FileMode.Open, FileAccess.Read, FileShare.Read),
                      //not mapping to a name
                      null,
                      //use the file's actual size
                      0L,
                      //read only access
                      MemoryMappedFileAccess.Read,
                      //adjust as needed
                      HandleInheritability.None,
                      //close the previously passed in stream when done
                      false);

        }

        /// <summary>
        /// Open a memory mapped file for read write.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static MemoryMappedFile GetReadWriteFile(string path)
        {
            return MemoryMappedFile.CreateFromFile(
                      //include a readonly shared stream
                      File.Open(path, FileMode.Open, FileAccess.ReadWrite, FileShare.None),
                      //not mapping to a name
                      null,
                      //use the file's actual size
                      0L,
                      //read write access
                      MemoryMappedFileAccess.ReadWrite,
                      //adjust as needed
                      HandleInheritability.None,
                      //close the previously passed in stream when done
                      false);

        }
    }
}
