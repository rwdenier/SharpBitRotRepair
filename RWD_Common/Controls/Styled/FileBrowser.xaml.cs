﻿/*  SharpBitRotRepair computes parity information, and if the original file is corrupted can try to repair it.
    Copyright (C) 2022 Robert Denier

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
using RWD_Common.Misc.Constants;
using System.Windows;
using System.Windows.Controls;

namespace RWD_Common.Controls.Styled
{
    public partial class FileBrowser : UserControl
    {
        public FileBrowser()
        {
            InitializeComponent();
            this.grid.Margin = CommonConstants.DefaultMargin;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var folderDialog = new Ookii.Dialogs.Wpf.VistaFolderBrowserDialog();
            var folderResult = folderDialog.ShowDialog();
            if (folderResult.HasValue && folderResult.Value)
            {
                this.File = folderDialog.SelectedPath;
            }
        }

        #region Properties
        /// <summary>
        /// Set the Icon with a png from RWD_Common/Resources that is built as a resource.
        /// </summary>
        public string File
        {
            get { return (string)GetValue(FileProperty); }
            set { SetValue(FileProperty, value); }
        }
        #endregion

        #region Dependency Properties
        public static readonly DependencyProperty FileProperty
            = DependencyProperty.Register("File", typeof(string), typeof(FileBrowser),
                new FrameworkPropertyMetadata(null, 
                    FrameworkPropertyMetadataOptions.AffectsRender|
                    FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        #endregion
    }
}
