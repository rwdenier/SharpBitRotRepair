﻿/*  SharpBitRotRepair computes parity information, and if the original file is corrupted can try to repair it.
    Copyright (C) 2022 Robert Denier

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
using ICSharpCode.AvalonEdit;
using NLog;
using NLog.Targets;
using System;
using System.Windows.Threading;

namespace RWD_Common.Controls.Logging
{
    [Target("AvalonEdit")]
    public class AvalonEditTarget : TargetWithLayout
    {
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();
        private TextEditor textEditor = null;
        public AvalonEditTarget(TextEditor textEditor)
        {
            this.textEditor = textEditor;
        }

        protected override void Write(LogEventInfo logEvent)
        {
            if (textEditor == null) return;

            string message = DateTime.Now.ToString("HH:mm:ss.f") + " " + logEvent.Level + ": " + logEvent.Message + "\r\n";

            textEditor.Dispatcher.BeginInvoke(
                DispatcherPriority.Background,
                new Action(() => this.textEditor.AppendText(message))
                );
            textEditor.Dispatcher.BeginInvoke(
                DispatcherPriority.Background,
                new Action(() => this.textEditor.ScrollToEnd())
                );

        }
    }
}
