﻿/*  SharpBitRotRepair computes parity information, and if the original file is corrupted can try to repair it.
    Copyright (C) 2022 Robert Denier

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
using NLog;
using NLog.Config;

namespace RWD_Common.Controls.Logging
{
    public class VisualStudioOutputLogging
    {

        public static void Enable()
        {
#if DEBUG
            LogManager.EnableLogging();
            LogManager.Configuration = new LoggingConfiguration();
            var logconsole = new NLog.Targets.DebuggerTarget("debuglog");
            var sentinalRule = new LoggingRule("*", LogLevel.Trace, logconsole);
            LogManager.Configuration.AddTarget("sentinal", logconsole);
            LogManager.Configuration.LoggingRules.Add(sentinalRule);
            LogManager.ReconfigExistingLoggers();
#endif
        }

    }
}
