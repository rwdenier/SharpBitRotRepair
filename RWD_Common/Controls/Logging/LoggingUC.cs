﻿/*  SharpBitRotRepair computes parity information, and if the original file is corrupted can try to repair it.
    Copyright (C) 2022 Robert Denier

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
using ICSharpCode.AvalonEdit;
using ICSharpCode.AvalonEdit.Highlighting.Xshd;
using NLog;
using NLog.Config;
using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Resources;
using System.Xml;

namespace RWD_Common.Controls.Logging
{
    public class LoggingUC : TextEditor
    {
        public LoggingUC()
        {
            this.FontSize = 13;
            this.FontFamily = new System.Windows.Media.FontFamily("Cascadia Code");
            this.Background = Brushes.Black;
            this.Foreground = Brushes.GhostWhite;
            this.TextArea.TextView.LinkTextBackgroundBrush = Brushes.Transparent;
            this.TextArea.TextView.LinkTextForegroundBrush = Brushes.Turquoise;
            this.TextArea.TextView.LinkTextUnderline = true;
            this.WordWrap = true;
            this.HorizontalScrollBarVisibility = ScrollBarVisibility.Hidden;
            LogManager.EnableLogging();
            LogManager.Configuration = new LoggingConfiguration();
            AvalonEditTarget avalonEditTarget = new AvalonEditTarget(this);
            var sentinalRule = new LoggingRule("*", LogLevel.Trace, avalonEditTarget);
            LogManager.Configuration.AddTarget("sentinal", avalonEditTarget);
            LogManager.Configuration.LoggingRules.Add(sentinalRule);
            LogManager.ReconfigExistingLoggers();
            LoadSyntaxHighlighting();
        }

        private void LoadSyntaxHighlighting()
        {
            Uri uri = new Uri("/RWD_Common;component/Controls/Logging/Logging.xshd", UriKind.Relative);
            StreamResourceInfo resourceInfo = Application.GetResourceStream(uri);
            Stream xshd_stream = resourceInfo.Stream;
            using (XmlTextReader reader = new XmlTextReader(xshd_stream))
            {
                this.SyntaxHighlighting = HighlightingLoader.Load(reader, ICSharpCode.AvalonEdit.Highlighting.HighlightingManager.Instance);
            }
            xshd_stream.Close();
        }

    }
}
