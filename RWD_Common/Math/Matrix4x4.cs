﻿/*  SharpBitRotRepair computes parity information, and if the original file is corrupted can try to repair it.
    Copyright (C) 2022 Robert Denier

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
using System;
using System.Runtime.CompilerServices;

namespace RWD_Common.Math
{
    /// <summary>
    /// The class can be lossless.  It does this by not fully taking inverses.
    /// Later other operations will take this into account and preserve the lossless operation.
    /// This is based on the example at.
    /// https://stackoverflow.com/questions/1148309/inverting-a-4x4-matrix
    /// </summary>
    public class Matrix4x4
    {
        public Matrix4x4()
        {
            this.inverseDetPresent = false;
        }
        public Matrix4x4(
            Int64 m00, Int64 m01, Int64 m02, Int64 m03, Int64 m10, Int64 m11, Int64 m12, Int64 m13, 
            Int64 m20, Int64 m21, Int64 m22, Int64 m23, Int64 m30, Int64 m31, Int64 m32, Int64 m33)
        {
            this.m00 = m00; this.m01 = m01; this.m02 = m02; this.m03 = m03;
            this.m10 = m10; this.m11 = m11; this.m12 = m12; this.m13 = m13;
            this.m20 = m20; this.m21 = m21; this.m22 = m22; this.m23 = m23;
            this.m30 = m30; this.m31 = m31; this.m32 = m32; this.m33 = m33;
            this.inverseDetPresent = false;
        }

        #region Variables
        public Int64 m00;
        public Int64 m01;
        public Int64 m02;
        public Int64 m03;
        public Int64 m10;
        public Int64 m11;
        public Int64 m12;
        public Int64 m13;
        public Int64 m20;
        public Int64 m21;
        public Int64 m22;
        public Int64 m23;
        public Int64 m30;
        public Int64 m31;
        public Int64 m32;
        public Int64 m33;
        /// <summary>
        /// This is the value before the inverse of the determinate to save precision.
        /// </summary>
        public Int64 inverseDet;
        /// <summary>
        /// This tells that the inverseDet value must be applied.
        /// </summary>
        private bool inverseDetPresent;

        Int64 A2323;
        Int64 A1323;
        Int64 A1223;
        Int64 A0323;
        Int64 A0223;
        Int64 A0123;
        Int64 A2313;
        Int64 A1313;
        Int64 A1213;
        Int64 A2312;
        Int64 A1312;
        Int64 A1212;
        Int64 A0313;
        Int64 A0213;
        Int64 A0312;
        Int64 A0212;
        Int64 A0113;
        Int64 A0112;
        #endregion

        public void CreateIdentityMatrix()
        {
            m00 = 1; m01 = 0; m02 = 0; m03 = 0;
            m10 = 0; m11 = 1; m12 = 0; m13 = 0;
            m20 = 0; m21 = 0; m22 = 1; m23 = 0;
            m30 = 0; m31 = 0; m32 = 0; m33 = 1;
        }

        public override string ToString()
        {
            string s1 = "" + m00 + ", " + m01 + ", " + m02 + ", " + m03;
            string s2 = "" + m10 + ", " + m11 + ", " + m12 + ", " + m13;
            string s3 = "" + m20 + ", " + m21 + ", " + m22 + ", " + m23;
            string s4 = "" + m30 + ", " + m31 + ", " + m32 + ", " + m33;
            string s5 = "";
            if (this.inverseDetPresent)
            {
                s5 = " InverseDet=" + this.inverseDet;
            }
            return "[" + s1 + "]\r\n[" + s2 + "]\r\n[" + s3 + "]\r\n[" + s4 + "]" + s5;
        }

        /// <summary>
        /// This converts the result to double, which is lossly, but it is just to check the result.
        /// </summary>
        /// <returns></returns>
        public string ToDecimalString()
        {
            if (!this.inverseDetPresent)
            {
                return ToString();
            }
            double d = this.inverseDet;

            string s1 = "" + m00/d + ", " + m01/d + ", " + m02 / d + ", " + m03 / d;
            string s2 = "" + m10/d + ", " + m11/d + ", " + m12 / d + ", " + m13 / d;
            string s3 = "" + m20/d + ", " + m21/d + ", " + m22 / d + ", " + m23 / d;
            string s4 = "" + m30/d + ", " + m31/d + ", " + m32 / d + ", " + m33 / d;
            return "[" + s1 + "]\r\n[" + s2 + "]\r\n[" + s3 + "]\r\n[" + s4 + "]";
        }



        private static bool CheckIt(ref Int64 x, Int64 inverseDet)
        {
            if (inverseDet == 0)
            {
                return false;
            }
            Int64 y = x;
            //Updated x
            x = x / inverseDet;
            if ((x * inverseDet) == y)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// This uses the same code as the inverse.
        /// </summary>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void CalcDeterminate()
        {
            this.A2323 = m22 * m33 - m23 * m32;
            this.A1323 = m21 * m33 - m23 * m31;
            this.A1223 = m21 * m32 - m22 * m31;
            this.A0323 = m20 * m33 - m23 * m30;
            this.A0223 = m20 * m32 - m22 * m30;
            this.A0123 = m20 * m31 - m21 * m30;
            this.A2313 = m12 * m33 - m13 * m32;
            this.A1313 = m11 * m33 - m13 * m31;
            this.A1213 = m11 * m32 - m12 * m31;
            this.A2312 = m12 * m23 - m13 * m22;
            this.A1312 = m11 * m23 - m13 * m21;
            this.A1212 = m11 * m22 - m12 * m21;
            this.A0313 = m10 * m33 - m13 * m30;
            this.A0213 = m10 * m32 - m12 * m30;
            this.A0312 = m10 * m23 - m13 * m20;
            this.A0212 = m10 * m22 - m12 * m20;
            this.A0113 = m10 * m31 - m11 * m30;
            this.A0112 = m10 * m21 - m11 * m20;

            this.inverseDet = m00 * (m11 * A2323 - m12 * A1323 + m13 * A1223)
                      - m01 * (m10 * A2323 - m12 * A0323 + m13 * A0223)
                      + m02 * (m10 * A1323 - m11 * A0323 + m13 * A0123)
                      - m03 * (m10 * A1223 - m11 * A0223 + m12 * A0123);
            //The original code had this, but of course since these are integers, that would be a bad idea
            //This must remain lossless.
            //det = 1 / inverseDet;
        }

        /// <summary>
        /// This does not complete the complete inverse.  
        /// The inverse of the determinate is not applied to avoid
        /// loss of precision in some (most) cases.
        /// </summary>
        /// <returns></returns>
        public Matrix4x4 PartialInverse()
        {
            CalcDeterminate();
            if (this.inverseDet == 0) return null;

            //These were multiplied by det previously, which previously was the inverse.
            //Unfortunately dividing by det in these next ones will cause data loss,
            //in some cases so there is no help but to just return det as well.
            Matrix4x4 y = new Matrix4x4();
            y.m00 = (m11 * A2323 - m12 * A1323 + m13 * A1223);
            y.m01 = -(m01 * A2323 - m02 * A1323 + m03 * A1223);
            y.m02 = (m01 * A2313 - m02 * A1313 + m03 * A1213);
            y.m03 = -(m01 * A2312 - m02 * A1312 + m03 * A1212);
            y.m10 = -(m10 * A2323 - m12 * A0323 + m13 * A0223);
            y.m11 = (m00 * A2323 - m02 * A0323 + m03 * A0223);
            y.m12 = -(m00 * A2313 - m02 * A0313 + m03 * A0213);
            y.m13 = (m00 * A2312 - m02 * A0312 + m03 * A0212);
            y.m20 = (m10 * A1323 - m11 * A0323 + m13 * A0123);
            y.m21 = -(m00 * A1323 - m01 * A0323 + m03 * A0123);
            y.m22 = (m00 * A1313 - m01 * A0313 + m03 * A0113);
            y.m23 = -(m00 * A1312 - m01 * A0312 + m03 * A0112);
            y.m30 = -(m10 * A1223 - m11 * A0223 + m12 * A0123);
            y.m31 = (m00 * A1223 - m01 * A0223 + m02 * A0123);
            y.m32 = -(m00 * A1213 - m01 * A0213 + m02 *A0113);
            y.m33 = (m00 * A1212 - m01 * A0212 + m02 * A0112);

            //It is necessary to eventually finish this calculation,
            //but since there is the possibility of loss, for now this will simply be carried along.
            y.inverseDet = this.inverseDet;
            y.inverseDetPresent = true;
            return y;
        }

        /// <summary>
        /// For regular matrix math, this is straightforward.
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        public Column4x1 Multiply(Column4x1 c)
        {
            Column4x1 ans;
            ans.valid = true;
            ans.p = m00 * c.p + m01 * c.q + m02 * c.r + m03 * c.s;
            ans.q = m10 * c.p + m11 * c.q + m12 * c.r + m13 * c.s;
            ans.r = m20 * c.p + m21 * c.q + m22 * c.r + m23 * c.s;
            ans.s = m30 * c.p + m31 * c.q + m32 * c.r + m33 * c.s;
            if (this.inverseDetPresent)
            { //We have a det, but ?
                //Ideally we should be able to divide by the det now, without losing precision.

                ans.valid =
                    CheckIt(ref ans.p, this.inverseDet) &&
                    CheckIt(ref ans.q, this.inverseDet) &&
                    CheckIt(ref ans.r, this.inverseDet) &&
                    CheckIt(ref ans.s, this.inverseDet);
            }
            return ans;
        }


            private Column4x1 Multiply(Column4x1 c, bool calc_deter=false)
        {
            if (calc_deter)
            {
                CalcDeterminate();
            }
            //Det needs to be divided at some point.
            //Fortunately it is a common factor.
            Column4x1 ans;
            ans.p = m00 * c.p + m01 * c.q + m02 * c.r + m03 * c.s;
            ans.q = m10 * c.p + m11 * c.q + m12 * c.r + m13 * c.s;
            ans.r = m20 * c.p + m21 * c.q + m22 * c.r + m23 * c.s;
            ans.s = m30 * c.p + m31 * c.q + m32 * c.r + m33 * c.s;

            bool valid = true;
            if (!CheckIt(ref ans.p, inverseDet)) valid = false;
            if (!CheckIt(ref ans.q, inverseDet)) valid = false;
            if (!CheckIt(ref ans.r, inverseDet)) valid = false;
            if (!CheckIt(ref ans.s, inverseDet)) valid = false;
            ans.valid = valid;
            return ans;
        }


    }
}
