﻿/*  SharpBitRotRepair computes parity information, and if the original file is corrupted can try to repair it.
    Copyright (C) 2022 Robert Denier

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
using System;
using System.Collections.Generic;

namespace RWD_Common.Math
{
    public struct Column4x1
    {
        public bool valid;
        public Int64 p;//MAX: 00 00 00 1F FF FF FF E0 - 3 bytes fully unused - 3 bits
        public Int64 q;//MAX: 00 00 02 0F FF FF FD F0 - 2 bytes fully unused - 6 bits
        public Int64 r;//MAX: 00 00 2C AF FF FF D3 50 - 2 bytes fully unused - 2 bits
        public Int64 s;//MAX: 00 04 40 FF FF FB BF 00 = 1 byte  fully unused - 4 bits

        /* Note:  When writing to disk, we can clearly skip the 8 bytes that are always 00.
         *        Before removal this would be 32 bytes.
         *        After removal this would be 24 bytes, so 25% less.
         *        
         *        There are an additional 14 bits that could be skipped, but probably not worth the added complexity.
         *        Most of the savings is accounted for in the 8 bytes.
         *        
         *        ToString() below can do a bit better.
         *        p_sum - X10
         *        q_sum - X11
         *        r_sum - X12
         *        s_sum - X13
         **/

        //Requires C#8 (Works with .NET 6)
        //public Column4x1()
        //{
        //    this.p = 0;
        //    this.q = 0;
        //    this.r = 0;
        //    this.s = 0;
        //    this.valid = false;
        //}

        public Column4x1(Int64 p, Int64 q, Int64 r, Int64 s)
        {
            this.p = p;
            this.q = q;
            this.r = r;
            this.s = s;
            this.valid = true;
        }

        public void Clear()
        {
            this.p = 0;
            this.q = 0;
            this.r = 0;
            this.s = 0;
        }

        public override string ToString()
        {
            return this.p.ToString("X10") + " " + this.q.ToString("X11") + " " + this.r.ToString("X12") + " " + this.s.ToString("X13");
        }

        /// <summary>
        /// Verifies two Column4x1s are equal.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (obj is Column4x1)
            {
                Column4x1 col2 = (Column4x1)obj;
                if (this.p != col2.p) return false;
                if (this.q != col2.q) return false;
                if (this.r != col2.r) return false;
                if (this.s != col2.s) return false;
                return true;
            }
            return false;
        }

        public override int GetHashCode()
        {
            Int64 hash = this.p ^ this.q ^ this.r ^ this.s;
            if (hash < 0) hash *= -1;
            UInt64 h = (UInt64)hash;
            h = ((h & 0xFFFFFFFF00000000) >> 32) ^ (h & 0xFFFFFFFF);
            return (int)h;
        }

        /// <summary>
        /// This is used in a repair to eliminate solutions that are not possible.
        /// </summary>
        /// <returns></returns>
        public bool CheckIfPossiblyValidData()
        {
            if (!this.valid) return false;
            if (this.p < 0 || this.q < 0 || this.r < 0 || this.s < 0) return false;
            if (this.p > UInt32.MaxValue || this.q > UInt32.MaxValue ||
                this.r > UInt32.MaxValue || this.s > UInt32.MaxValue) return false;
            return true;
        }

        public string GetMismatchString(Column4x1 other, int [] indexes, out int count)
        {
            List<int> locations = new List<int>();
            if (this.p != other.p) locations.Add(indexes[0]);
            if (this.q != other.q) locations.Add(indexes[1]);
            if (this.r != other.r) locations.Add(indexes[2]);
            if (this.s != other.s) locations.Add(indexes[3]);
            locations.Sort();
            count = locations.Count;
            return string.Join(',',locations);
        }
    }
}
